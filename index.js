db.fruits.aggregate([
        {$match:{$and:[{supplier:"Yellow Farms"},{price:{$lt:50}}]}},
        {$count:"yellowFarmsLowerThan50"}
    ])

db.fruits.aggregate([
    {$match:{price:{$lt:30}}},
    {$count: "priceLessThan30"}
])

db.fruits.aggregate([
    {$match:{supplier:"Yellow Farms"}},
    {$group:{_id:"Yellow Farms",avgPrice:{$avg:"$price"}}}
])

db.fruits.aggregate([
    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"Red Farms Inc.",maxPrice:{$max:"$price"}}}
])

db.fruits.aggregate([
    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"Red Farms Inc.",minPrice:{$min:"$price"}}}
])